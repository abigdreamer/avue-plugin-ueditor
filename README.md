## avue 富文本编辑器
<p align="center">
  <img width="600" src="https://gitee.com/smallweigit/avue-plugin-ueditor/raw/master/packages/demo/demo.png">
</p>

## Avue官网
[https://avuejs.com](https://avuejs.com)

## 介绍
- [demo](https://avuejs.com/doc/plugins/ueditor-plugins)
- [npm](https://www.npmjs.com/package/avue-plugin-ueditor)
- [git](https://gitee.com/smallweigit/avue-plugin-ueditor)

## 三方编辑器wangEditor

- [文档](https://doc.wangeditor.com/)
- 组件配置中customConfig属性为wangEditor编辑器的配置

## 使用
```
1.安装
npm install avue-plugin-ueditor --save

2.导入
import AvueUeditor from 'avue-plugin-ueditor'
Vue.use(AvueUeditor);

3.Avue配置中使用
...
column:[
  ...
    {
      label:'test',
      prop:'test',
      component: "avueUeditor",
      //wangEditor编辑的配置
      customConfig: {
        lineHeights: ['1', '1.15', '1.6', '2', '2.5', '3']
      },
      //是否启用oss(qiniu/ali)
      oss:'',
      headers:{},
      data:{},
      propsHttp: {
        home:'',
        url:'',
        res: ''
      },
      //普通图片上传
      action: "",
      //七牛云oss配置
      qiniu: {
        AK: "",
        SK: "",
        scope: "",
        url: "",
        deadline: 1
      },
      //阿里云oss配置
      ali: {
        region: "",
        endpoint: "",
        accessKeyId: "",
        accessKeySecret: "",
        bucket: ""
      }
    }
  ...
]
4.组件中使用
<avue-ueditor v-model="text" v-bind="options"></avue-ueditor>

options: {
  //wangEditor编辑的配置
  customConfig: {
    lineHeights: ['1', '1.15', '1.6', '2', '2.5', '3']
  },
  propsHttp: {
    home:'',
    url:'',
    res: ''
  },
  //是否启用oss(qiniu/ali)
  oss:'',
  headers:{},
  data:{},
  //普通图片上传
  action: "",
  //七牛云oss配置
  qiniu: {
    AK: "",
    SK: "",
    scope: "",
    url: "",
    deadline: 1
  },
  //阿里云oss配置
  ali: {
    region: "",
    endpoint: "",
    accessKeyId: "",
    accessKeySecret: "",
    bucket: ""
  }
}
...
```

## upload Attributes
| 参数    | 说明                              | 类型   | 可选值    | 默认值 |
| ------- | --------------------------------- | ------ | --------- | ------ |
| action  | 图片上传地址                      | String | —         | -      |
| headers | 上传附带请求头数据                | Object | —         | -      |
| data    | 上传附带数据                      | Object | —         | -      |
| props   | 相关可配置参数，具体参考props配置 | Object | —         | -      |
| oss     | 是否使用oss不写则为普通上传       | String | ali/qiniu | -      |
| qiniu   | 七牛云oss配置，具体参考qiniu配置  | Object | —         | -      |
| ali     | 阿里云oss配置，具体参考ali配置    | Object | —         | -      |

## propsHttp Attributes
| 参数 | 说明                   | 类型   | 可选值 | 默认值 |
| ---- | ---------------------- | ------ | ------ | ------ |
| res  | 返回的数据结构层次     | String | —      | -      |
| url  | 返回结构体图片地址字段 | String | —      | -      |
| home | 图片的根路径地址       | String | —      | -      |

## qiniu Attributes
| 参数     | 说明               | 类型   | 可选值 | 默认值 |
| -------- | ------------------ | ------ | ------ | ------ |
| AK       | 七牛云的密钥       | String | —      | -      |
| SK       | 七牛云的密钥       | String | —      | -      |
| scope    | 七牛云存储的空间名 | String | —      | -      |
| url      | 空间的自定义域名   | String | —      | -      |
| deadline | token的过期时间    | String | —      | -      |

## ali Attributes
| 参数            | 说明                                     | 类型   | 可选值 | 默认值 |
| --------------- | ---------------------------------------- | ------ | ------ | ------ |
| region          | oss所在区域的名称，具体查看阿里云oss文档 | String | —      | -      |
| endpoint        | oss所在区域的域名，具体查看阿里云oss文档 | String | —      | -      |
| accessKeyId     | 阿里云的密钥                             | String | —      | -      |
| accessKeySecret | 阿里云的密钥                             | String | —      | -      |
| bucket          | 阿里云存储的空间名                       | String | —      | -      |